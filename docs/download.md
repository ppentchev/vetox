<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Download

These are the released versions of [vetox](index.md) available for download.

## [0.2.1] - 2025-01-18

### Source tarball

- [vetox-0.2.1.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.1.tar.gz.asc))

### Python wheel

- [vetox-0.2.1-py3-none-any.whl](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.1-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.1-py3-none-any.whl.asc))

## [0.2.0] - 2024-08-08

### Source tarball

- [vetox-0.2.0.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.0.tar.gz.asc))

### Python wheel

- [vetox-0.2.0-py3-none-any.whl](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.0-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.2.0-py3-none-any.whl.asc))

## [0.1.4] - 2024-07-18

### Source tarball

- [vetox-0.1.4.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.gz.asc))
- [vetox-0.1.4.tar.bz2](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.bz2.asc))
- [vetox-0.1.4.tar.xz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.4.tar.xz.asc))

## [0.1.3] - 2024-03-15

### Source tarball

- [vetox-0.1.3.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.gz.asc))
- [vetox-0.1.3.tar.bz2](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.bz2.asc))
- [vetox-0.1.3.tar.xz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.3.tar.xz.asc))

## [0.1.2] - 2024-02-03

### Source tarball

- [vetox-0.1.2.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.gz.asc))
- [vetox-0.1.2.tar.bz2](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.bz2.asc))
- [vetox-0.1.2.tar.xz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.2.tar.xz.asc))

## [0.1.1] - 2024-02-03

### Source tarball

- [vetox-0.1.1.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.gz.asc))
- [vetox-0.1.1.tar.bz2](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.bz2.asc))
- [vetox-0.1.1.tar.xz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.1.tar.xz.asc))

## [0.1.0] - 2023-12-21

### Source tarball

- [vetox-0.1.0.tar.gz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.gz.asc))
- [vetox-0.1.0.tar.bz2](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.bz2.asc))
- [vetox-0.1.0.tar.xz](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/devel/vetox/vetox-0.1.0.tar.xz.asc))

[0.2.1]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.2.1
[0.2.0]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.2.0
[0.1.4]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.1.4
[0.1.3]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.1.3
[0.1.2]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.1.2
[0.1.1]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.1.1
[0.1.0]: https://gitlab.com/ppentchev/vetox/-/tags/release%2F0.1.0
